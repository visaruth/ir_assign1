library(NLP)
library(tm)
docs <- Corpus(DirSource("M:/ir_assign1/taylor"))
writeLines(as.character(docs[[15]]))

toSpace <- content_transformer(function(x, pattern) {return (gsub(pattern, " ", x))})
docs <- tm_map(docs, toSpace, "-")
docs <- tm_map(docs, toSpace, ":")
docs <- tm_map(docs, toSpace, "\'")
docs <- tm_map(docs, toSpace, "\\?")
docs <- tm_map(docs, toSpace, ",")
docs <- tm_map(docs, toSpace, "\\.")

docs <- tm_map(docs, removePunctuation)
docs <- tm_map(docs,content_transformer(tolower))
docs <- tm_map(docs, removeNumbers)
docs <- tm_map(docs, removeWords, stopwords("english"))
docs <- tm_map(docs, stripWhitespace)

library(SnowballC)
docs <- tm_map(docs,stemDocument)
writeLines(as.character(docs[[15]]))

dtm <- DocumentTermMatrix(docs)
inspect(dtm[1:20,1:10])
fre <- colSums(as.matrix(dtm))

#for count term
length(fre)

ord <- order(fre,decreasing=TRUE)
fre[head(ord)]
fre[tail(ord)]
findFreqTerms(dtm,lowfreq=50)

wf=data.frame(term=names(fre),occurrences=fre)

library(ggplot2)

p <- ggplot(subset(wf, fre>50), aes(term, occurrences))
p <- p + geom_bar(stat="identity")
p <- p + theme(axis.text.x=element_text(angle=45, hjust=1))
p

wordcloud(names(fre),fre, min.fre=50)